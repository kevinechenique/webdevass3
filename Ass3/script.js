/**
 * @author Kevin Echenique Arroyo
 */

"use strict";

document.addEventListener("DOMContentLoaded", setup);

let globalObj = {};//We are making a global object so we can create global attributes later
function setup(){
    const button = document.querySelector('button');
    //console.log(section.children);
    button.addEventListener('click', fetchData);
}

/**
 * Retrieves the quotes using fetch and checls for any errors (if any) when
 * recoivinf the responses 
 */
function fetchData(e){
    e.preventDefault();
    const url = "https://ron-swanson-quotes.herokuapp.com/v2/quotes/";
    fetch(url)
        //fetch is goin to return to us a response
        //.then(res => console.log(res))//returns whether the response was sucessfull or not. We can write res or response
         //converting the response to JSON so it can be accessed by the response object. Returns a promise 
        .then(response => {
            if(response.ok){
            return response.json()
            }
            throw new Error("Status code: " + response.statusCode);
        })
        //gets the data from whithin the object. In this case it'll get the quotes
        //.then(data => console.log(data))
        .then(json => display(json))
        .catch(error => console.error("An error was found: " + error))
}

/**
 * Dislays the quotes fetched from the url to our HTML  
 */
function display(json){
    let section = document.querySelector('section');
    let quote = document.createElement('p');
    quote.textContent = json;
    section.appendChild(quote);
}
  